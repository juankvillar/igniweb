$('#frm').submit(function (e) { 
    e.preventDefault();
    let datos = $( this ).serializeArray();
        datos.push({name: 'opcn' , value: 'loguearse' })

    $.ajax({
        type: 'post',
        url: 'controllers/login.php',
        dataType: 'json',
        data: datos,
    }).done(function (data) {
        
        if( !data.error ){
            location.reload();
        }else{
            swal(data.msj,'',data.type);
        }
    })
    .fail(function (data) {
        swal('Error en la solicitud ','','error');
    })
});