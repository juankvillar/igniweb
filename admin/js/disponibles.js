$(document).ready(function () {
    init()
});

$('#frm').submit(function (e) { 
    e.preventDefault();
    let datos = $( this ).serializeArray();
        datos.push({name: 'opcn' , value: opcn })
        datos.push({name: 'asesor_id' , value: asesor_id })

    $.ajax({
        type: 'post',
        url: 'controllers/disponibles.php',
        dataType: 'json',
        data: datos,
    }).done(function (data) {
        swal(data.msj,'',data.type);
    })
    .fail(function (data) {
        swal('No se realizó ningún cambio','','error');
    })
});

let book_id = '';
$("body").on('click', '.detalle', function (e) {
      e.preventDefault();

      book_id = $( this ).data('id');
      
      $.ajax({
        type: 'POST',
        url: 'controllers/disponibles.php',
        dataType: 'json',
        data: {opcn: "detalleLibro", id_book:book_id},
    }).done(function (data) {

        $("#txtLibro").html(data.name);
        $("#txtAutor").html(data.autor);
        $("#txtAutor").html(data.autor);
        $('#imglibro').attr('src', '../resource/libros/'+data.image);
    })
    .fail(function (data) {
        
    })
  });

  $("body").on('click', '#btnreservar', function (e) {
    e.preventDefault();

    $.ajax({
      type: 'POST',
      url: 'controllers/disponibles.php',
      dataType: 'json',
      data: {opcn: "reservar", id_book:book_id},
  }).done(function (data) {
    swal(data.msj,'',data.type);
    $('#myModal').modal('hide');
    init();
  })
  .fail(function (data) {
      
  })
});
  


  function init( category = 0 ) {

    $.ajax({
        type: 'POST',
        url: 'controllers/disponibles.php',
        dataType: 'json',
        data: {opcn: 'init', category: category}
    }).done(function (data) {
        renderTable( data )
    })
    .fail(function (data) {
        swal('No se realizó ningún cambio','','error');
    })
  }

  function renderTable( datos ) {
    
    let tabla = '';

      datos.forEach(function (d, index) {
        tabla += `<tr >
                      <td>`+ d.libro + `</td>
                      <td>`+ d.category + `</td>
                      <td><img src="../resource/libros/`+ d.image + `" alt="imagenb libro"></td>
                      <td>  <button data-id=` + d.id_book + ` type="button" class="detalle btn btn-warning" data-toggle="modal" data-target="#myModal">Details</button> </td>
                  </tr>`;
      });
      $('#tbreservas').html(tabla);

  }


  $('#selCategory').change(function (e) { 
    e.preventDefault();
    let id_category = $( this ).val();
    init( id_category )
  });