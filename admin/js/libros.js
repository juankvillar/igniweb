$(document).ready(function () {
    init()
});

$('#frm').submit(function (e) { 
    e.preventDefault();
    let datos = $( this ).serializeArray();
        datos.push({name: 'opcn' , value: opcn })
        datos.push({name: 'asesor_id' , value: asesor_id })

    $.ajax({
        type: 'post',
        url: 'controllers/libros.php',
        dataType: 'json',
        data: datos,
    }).done(function (data) {
        swal(data.msj,'',data.type);
    })
    .fail(function (data) {
        swal('No se realizó ningún cambio','','error');
    })
});

$("body").on('click', '.devolver', function (e) {
      e.preventDefault();

      if( !confirm('¿Esta seguro de devolver este libro?')){ return false;}
      let id = $( this ).data('id');
      console.log(id)
      $.ajax({
        type: 'POST',
        url: 'controllers/libros.php',
        dataType: 'json',
        data: {opcn: "devolver", id_book:id},
    }).done(function (data) {
        swal(data.msj, '' , data.type);
        init()
    })
    .fail(function (data) {
        swal('No se realizó ningún cambio','','error');
    })
  });


  function init() {

    $.ajax({
        type: 'POST',
        url: 'controllers/libros.php',
        dataType: 'json',
        data: {opcn: 'init'}
    }).done(function (data) {
        renderTable( data.res );
        $('#txtCantLibros').text(data.cantLibros);
        $('#txtname').text(data.us.name);

    })
    .fail(function (data) {
        swal('No se realizó ningún cambio','','error');
    })
  }

  function renderTable( datos ) {
    
    let tabla = '';

      datos.forEach(function (d, index) {
        tabla += `<tr >
                      <td>`+ d.libro + `</td>
                      <td>`+ d.category + `</td>
                      <td><img src="../resource/libros/`+ d.image + `" alt="imagenb libro"></td>
                      <td> <button data-id=` + d.id_book + ` type="button" class="devolver btn btn-danger">Return</button> </td>
                  </tr>`;
      });
      $('#tbreservas').html(tabla);

  }