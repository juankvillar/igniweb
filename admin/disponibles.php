<?php include('controllers/disponibles.php'); ?>
<?php include('controllers/seguridad.php'); ?>
<?php include('../template/header.php'); ?>
<style>
  td img {
    width: 60px;
  }

  .modal-body img{
    width: 80%;
  }
</style>
<div class="container">
  <br>
  <!-- Modal -->
  <div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Information</h4>
        </div>
        <div class="modal-body">
          <div class="row">
            <div class="col-md-12">
              <p> <strong>Book title: </strong> <span id="txtLibro"></p>
              <p> <strong>Author:</strong>  <span id="txtAutor"></span></p>
            </div>
          </div>
          <hr>
          <div class="row">
            <div class="col-md-6">
              <p><strong>Description:</strong> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>
            </div>
            <div class="col-md-6">
              <img id="imglibro" src="" alt="imagenlibro">
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-warning" id="btnreservar" >Add</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
        </div>
      </div>

    </div>
  </div>

  <div>
    <span>Select category</span>
    <select style="width: 50%;" class="form-control" name="selCategory" id="selCategory">
      <option value="0">All</option>
      <?php 
        foreach ($category as $key => $value) {
      ?>
      <option value="<?php echo $value->id_category ?>"><?php echo $value->category ?></option>
      <?php } ?>
    </select>
  </div>
  <hr>
  <!-- Inicio listar empleados -->
  <table class="table table-striped table-hover">
    <thead>
      <tr>
        <th>Name</th>
        <th>Category</th>
        <th>Image</th>
        <th>Option</th>
      </tr>
    </thead>
    <tbody id="tbreservas">
    </tbody>
  </table>
</div>
<?php include('../template/footer.php'); ?>
<script>
  let opcn = "crear"
  let asesor_id = '';
</script>
<script src="js/disponibles.js?sin_cache=<?php echo md5(time()); ?>"></script>