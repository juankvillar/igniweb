<?php


Class Libros{

	public $con = NULL;
	public $usuario_id = NULL;

	public function __construct() {
		if( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' ){
			$ajax = '../';
		 }else{
			$ajax = '';
		 }
		include_once($ajax.'../config/init_db.php');
		$this->con = $mbd;
		$this->usuario_id = $usuario_id;
	}

	public function misReservas(){
		$query = "SELECT b.id_book, b.name libro, b.image, c.category FROM reserved_books r
							INNER JOIN users u
								on r.id_user = u.id_user and r.id_user = $this->usuario_id
							INNER JOIN books b
								on r.id_book = b.id_book
							INNER JOIN category c
								on b.id_category = c.id_category;";
		$res = $this->con->query($query);
        $asesores = $res->fetchAll();
		return $asesores;
	}

	public function getUser(){
		$query = "SELECT * FROM users where id_user = $this->usuario_id";
		$res = $this->con->query($query);
        $asesor = $res->fetch();
		return $asesor;
	}
		
	public  function devolverLibro( $id_book ){

		$query = "DELETE FROM reserved_books WHERE id_book = $id_book and id_user = $this->usuario_id;";
		$resultSet_usr = $this->con->exec($query);
            if ($resultSet_usr) {
                $respuesta['error'] = false;
                $respuesta['msj'] = 'Success';
                $respuesta['type'] = "success";
            } else {
                $respuesta['error'] = true;
                $respuesta['msj'] = 'No se realizó ningún cambio';
                $respuesta['type'] = "error";
            }
			return $respuesta;
	}

}

