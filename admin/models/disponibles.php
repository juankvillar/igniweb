<?php

Class Libros{

	public $con = NULL;
	public $usuario_id = NULL;

	public function __construct() {
		if( !empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' ){
			$ajax = '../';
		 }else{
			$ajax = '';
		 }
		include_once($ajax.'../config/init_db.php');
		$this->con = $mbd;
		$this->usuario_id = $usuario_id;
	}

	public function disponibles(){
		extract($_POST);
		$subQuery = $category != 0 ? " and c.id_category = $category " : "";
		

		$query = "SELECT b.id_book, b.name libro, b.image, c.category
						FROM books b
							INNER JOIN category c
								on b.id_category = c.id_category
								where id_book not in( select id_book from reserved_books ) $subQuery ;";
		$res = $this->con->query($query);
        $asesores = $res->fetchAll();
		return $asesores;
	}

	public function getCategories(){
		$query = "SELECT id_category, category
						FROM category;";
		$res = $this->con->query($query);
        $asesores = $res->fetchAll();
		return $asesores;
	}
	
	public function detalleLibro( $id_book ){
		$query = "SELECT b.id_book, b.name, b.autor, b.image, c.category
						FROM books b
							INNER JOIN category c
								on b.id_category = c.id_category
								where id_book = $id_book;";
		$res = $this->con->query($query);
        $asesores = $res->fetch();
		return $asesores;
	}
	

	public  function reservarLibro( $id_book ){
		
		$query = "INSERT INTO reserved_books
					(
					id_user,
					id_book
					)
					VALUES
					(
					'$this->usuario_id',
					'$id_book'
					);";
		$res = $this->con->query($query);
            if ($res) {
                $respuesta['error'] = false;
                $respuesta['msj'] = 'Sucsess';
                $respuesta['type'] = "success";
            } else {
                $respuesta['error'] = true;
                $respuesta['msj'] = 'No se pudo reservar el libro';
                $respuesta['type'] = "error";
            }
			return $respuesta;
	}
	
}

