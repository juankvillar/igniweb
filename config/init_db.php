<?php

// mostarr errores PHP
$desarrollo = true;
if ( $desarrollo ) {
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
}

// define zona horaria
date_default_timezone_set('America/Bogota');
set_time_limit(0);

// varia global con la fecha actual
@session_start();
$usuario_id =  isset($_SESSION['id_user']) ? $_SESSION['id_user'] : '' ;
define('FECHA', date('Y-m-d H:i:s'));
define('USUARIO_ID', $usuario_id);

// configuracion PDO
$options = array(PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_OBJ, PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING);

//conexion a la base de datos
$BDusuario    = 'root';
$BDcontrasena = '';
$BDbase_datos =  'igniweb';

$mbd = new PDO("mysql:host=localhost;dbname=$BDbase_datos", $BDusuario, $BDcontrasena, $options);
